---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

+++ {"deletable": false, "editable": false, "nbgrader": {"cell_type": "markdown", "checksum": "f25e42cb3977b3b0362c01a4e47423a7", "grade": false, "grade_id": "cell-a68a972bbd7d3141", "locked": true, "schema_version": 3, "solution": false, "task": false}}

# Assignment 1 : Basics

+++

## Question 1

Print Hello World using Python.

```{code-cell}
# enter your code here
```
